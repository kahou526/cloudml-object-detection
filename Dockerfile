FROM tensorflow/tensorflow

RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        git \
        unzip \
        wget \
        python-pil \
        python-lxml \
        python-tk

RUN pip install --user \
        Cython \
        contextlib2 \
        pillow \
        lxml \
        jupyter \
        matplotlib

WORKDIR /tensorflow
RUN git clone https://github.com/tensorflow/models.git

WORKDIR /tensorflow/models/research
RUN wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip && \
  unzip protobuf.zip && \
  ./bin/protoc object_detection/protos/*.proto --python_out=.

RUN git clone https://github.com/cocodataset/cocoapi.git && \
  cd cocoapi/PythonAPI && \
  make && \
  cp -r pycocotools /tensorflow/models/research/

RUN echo "export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim" >> /root/.bash_profile