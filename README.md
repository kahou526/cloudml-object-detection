# Object detection trainning with custom data

This project contains scripts and code snippet to train a object detection model with Tensorflow object detection API

## Requirements

- Google Cloud Platform account
- python 2.7+
- Anaconda 3
- LabelImg
- Tensorflow (We use docker here)

## Prepare trainning data

First of all, prepare image data in folder `data` and use `LabelImg` to label the images, `LabelImg` will give us `.xml` annotations with bounding boxes.
Once all the images are labeled, move `.xml` files into `data/annotations` will we need that on next step.

### Create tensorflow trainning record

__Docker setup__

As said, we use tensorflow docker to avoid polluting our system. We have prepared a `Dockerfile` contains essential setup of tensorflow objected detection trainning packages.

- Build tensorflow image `docker build -t tensorflow_od .`

__Convert XML to CSV__

[Details tutorial here](https://gist.github.com/douglasrizzo/c70e186678f126f1b9005ca83d8bd2ce)

- Prepare datasets CSV

```
# From data/ (folder with images)

python xml_to_csv.py
```

After that, randomly pick 10 - 20% entries from generated `.csv` file into a file `**_val.csv` and rename the original file into `**_train.csv`. That gives us trainning and valuation datasets.

- Generate tf record
  - With `.csv` pointing us to correct sets of data, we can us generate tf record with `generate_tf_record.py`. But we need some adjustment to the code to give us correct class label, replace code in function `class_text_to_int` to match your labels.
  - This script require tensorflow packages, so we need to run it in our tensorflow docker environment

  Let's start the docker container with 
  ```
  docker run -ti -v ${PWD}:/tf_data tensorflow_train bash
  # inside container
  cp /tf_data/generate_tf_record.py /tensorflow/models/research/

  or

  # Haven't try this yet, suppose to map the script to tensorflow folder
  docker run -ti -v ${PWD}:/tf_data -v ${PWD}/generate_tf_record.py:/tensorflow/models/research/ tensorflow_train bash
  ```

  Then generate the tfrecords

  ```
  python generate_tf_record.py --csv_input=/tf_data/data/[label]/**_val.csv --image_dir=/tf_data/data/[label] --output_path=/tf_data/[label]_val.record

  python generate_tf_record.py --csv_input=/tf_data/data/[label]/**_train.csv --image_dir=/tf_data/data/[label] --output_path=/tf_data/[label]_train.record
  ```

## Run the trainning on GCP

We use gcloud command line tool to access our Google Cloud Platform Resources, please install it in your machine beforehead [Install here](https://cloud.google.com/sdk/docs/quickstart-macos)

For easy access, please config the following environment variables or put them into your shell profile

```
export BUCKET_NAME=bucket_name
```

### Upload the TFRecord

CloudML use storage in trainning process, read datasets and write outputs. We should create a bucket for our trainning when there is none.

`gsutil mb -l $REGION gs://$BUCKET_NAME`

Then use `gsutil` to upload our TFRecords

```
gsutil cp *.record gs://$BUCKET_NAME
```

### Upload label path text file

Define all the lables in your trainning data in a file named `**_label_map.pbtxt` with the following format

```
item {
  id: 1
  name: 'car'
}
item {
  id: 2
  name: 'table'
}
...
```

Then upload to storage

`gsutil cp **_label_map.pbtxt gs://$BUCKET_NAME`

### Upload fine tune checkpoints

When transfer learning from existing model, we need to upload that to storage for CloudML. We use [MobileNet SSD v2](http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v2_coco_2018_03_29.tar.gz)
More model can be find in [Tensorflow model zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md)

`gsutil cp -r ssd_mobilenet_v2_coco gs://$BUCKET_NAME`

### Object detection pipeline configuration

Please check `ssd_mobilenet_v2_coco.config` and update the following fields to your file location

```
fine_tune_checkpoint: gs://bucket_name/model_folder/model.ckpt

...

train_input_reader: {
  tf_record_input_reader {
    input_path: "gs://bucket_name/train.record
  }
  label_map_path: "gs://bucket_name/**_label_map.pbtxt"
}

...

eval_input_reader: {
  tf_record_input_reader {
    input_path: "gs://bucket_name/val.record"
  }
  label_map_path: "gs://bucket_name/**_label_map.pbtxt"
  shuffle: false
  num_readers: 1
}
```

Now we can follow the instructions [here](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/running_on_cloud.md)

```
# From tensorflow/models/research/
gcloud ml-engine jobs submit training object_detection_`date +%m_%d_%Y_%H_%M_%S` \
    --runtime-version 1.9 \
    --job-dir=gs://$BUCKET_NAME/car_training \
    --packages dist/object_detection-0.1.tar.gz,slim/dist/slim-0.1.tar.gz,pycocotools/pycocotools-2.0.tar.gz \
    --module-name object_detection.model_main \
    --region us-east1 \
    --config ${PWD}/cloud_ml.yaml \
    -- \
    --model_dir=gs://$BUCKET_NAME/car_training \
    --pipeline_config_path=gs://$BUCKET_NAME/ssd_mobilenet_v2_coco.config
```

```
gcloud ml-engine jobs submit training object_detection_`date +%m_%d_%Y_%H_%M_%S` \
    --runtime-version 1.11 \
    --job-dir=gs://$BUCKET_NAME_US/car_training \
    --packages dist/object_detection-0.1.tar.gz,slim/dist/slim-0.1.tar.gz,pycocotools/pycocotools-2.0.tar.gz \
    --module-name object_detection.model_tpu_main \
    --region us-central1 \
    --scale-tier BASIC_TPU \
    -- \
    --model_dir=gs://$BUCKET_NAME_US/car_training \
    --pipeline_config_path=gs://$BUCKET_NAME_US/ssd_mobilenet_v2_coco_cloudml.config
```

```
gcloud ml-engine jobs submit training object_detection_`date +%m_%d_%Y_%H_%M_%S` \
    --runtime-version 1.11 \
    --job-dir=gs://$BUCKET_NAME_US/car_training \
    --packages dist/object_detection-0.1.tar.gz,slim/dist/slim-0.1.tar.gz,pycocotools/pycocotools-2.0.tar.gz \
    --module-name object_detection.model_main \
    --region us-central1 \
    --config ${PWD}/cloud_ml.yaml \
    -- \
    --model_dir=gs://$BUCKET_NAME_US/car_training \
    --pipeline_config_path=gs://$BUCKET_NAME_US/ssd_mobilenet_v2_coco_cloudml.config
```
